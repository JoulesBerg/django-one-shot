from django.shortcuts import render
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

from todos.models import TodoItem, TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    context_object_name = "todo_lists"
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    context_object_name = "todo_list"
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        return redirect("todo_list_detail", pk=new.id)


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        return redirect("todo_list_detail", pk=new.id)


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/itemcreate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def form_valid(self, form):
        new = form.save(commit=False)
        connectedlist = new.list
        new.save()
        return redirect("todo_list_detail", pk=connectedlist.id)


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/itemupdate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def form_valid(self, form):
        new = form.save(commit=False)
        connectedlist = new.list
        new.save()
        return redirect("todo_list_detail", pk=connectedlist.id)
